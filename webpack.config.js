var HtmlWebpackPlugin = require('html-webpack-plugin');
var webpack = require('webpack');
var fileloader = require('file-loader');

//var file_load = require("file-loader?emitFile=false!./ball.png");
//var img_ball = require ('./src/image/ball.png');

const PATHS = {
  imagesLoc: {value: './image'}
};

module.exports = {

    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.js', '.json', '.ts','.png']
    },

    entry: {
        app: './src/main.ts',
    },

    plugins: [
        new HtmlWebpackPlugin({
            title: 'Breakout',
            template: 'index.template.ejs'
        })
    ],

    output: {
        path: 'output',
        filename: 'breakout.js',
    },
    module: {
        loaders: [
            {
            test: /\.(jpg|jpeg|gif|png|svg)$/,
            exclude: /node_modules/,
            //include: './image/',
            loader: 'file-loader',
            options: {
                name: './images/[hash].[ext]',
                }
            },
            {
            test: /\.ts$/,
            exclude: /node_modules/,
            loader: 'babel-loader!ts-loader'
            }
            ]
    }
};

/*

{
       test: /\.(jpg|jpeg|gif|png|svg)$/,
       exclude: /node_modules/,
       include: PATHS.imagesLoc,
       loader: "file-loader?name=img/[name].[ext]"
   }
, {
            test: /\.(jpe?g|png|gif|svg)$/,
            loader: 'file?hash=sha512&digest=hex&name=[hash].[ext]!
            image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
            } 
   {
            test: /\.png$/,
            exclude: /node_modules/,
            loader: 'file-loader?name=[path][name].[ext]?[hash:10]'    
            },

{
  test: /\.(jpe?g|png|gif|svg)(\?v=\d+\.\d+\.\d+)?$/i,
  exclude: /(node_modules|bower_components)/,
  loader: 'file',
  query: {
    regExp: '\\b(assets.+)',
    name: '[1]?[hash:10]',
  },
},


,
            {
            test: /\.png$/,
            exclude: /node_modules/,
            loader: 'file-loader?name=./src/image/ball.png'
            //file-loader?name=./image/ball.png
        }
*/