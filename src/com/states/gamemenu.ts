/*
Author: jose.ii.y.de.vera
Class Description: gamenu class is 3rd state class that 
displays backgrounds and message menu of the game.
*/
class gamemenu extends Phaser.State {
 
        public title: Phaser.Sprite;
        public logo: Phaser.Sprite;
 
        create() {
 
            this.title = this.add.sprite(this.game.world.centerX, 100, 'title');
            this.title.anchor.setTo(0.5, 0.5);
            this.title.alpha = 0;
 
            this.logo = this.add.sprite(this.game.world.centerX, -300, 'logo');
            this.logo.anchor.setTo(0.5, 0.5);
 
            this.add.tween(this.title).to({ alpha: 1}, 2000, Phaser.Easing.Bounce.InOut, true);
            this.add.tween(this.logo).to({ y: 250 }, 2000, Phaser.Easing.Elastic.Out, true, 2000);
            
            this.input.onDown.addOnce(this.fadeOut, this);
 
        }
 
        fadeOut() {
 
            this.add.tween(this.title).to({ alpha: 0 }, 2000, Phaser.Easing.Linear.None, true);
            var tween = this.add.tween(this.logo).to({ y: 800 }, 2000, Phaser.Easing.Linear.None, true);
 
            tween.onComplete.add(this.startGame, this);
 
        }
         
        startGame() {
 
            this.game.state.start('level1', true, false);
 
        }
 }
 export { gamemenu };