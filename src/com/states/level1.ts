/*
Author: jose.ii.y.de.vera
Class Description: gamenu class is 4th state class that 
controls the logic and rules of the objects in the game.
*/

import {paddle} from '../../com/objects/paddle';
import {capsule} from '../../com/objects/capsule';
import {player} from '../../com/objects/player';
import {virus} from '../../com/objects/virus';


 class level1 extends Phaser.State {

        private background: Phaser.Sprite;
        private capsuleOnPaddle: boolean = true;
        private paddle_object: paddle;
        private capsule_object: capsule;
        private player_object: player;
        private virus_object: virus;
        private insect_group: any;


        create() {
        this.background = this.add.sprite(0, 0, 'bg');
        this.background.height = this.game.height;
        this.background.width = this.game.width;
        this.background.physicsType = Phaser.Physics.ARCADE;

        this.physics.startSystem(Phaser.Physics.ARCADE);
        this.physics.arcade.checkCollision.down = false;

        this.paddle_object = new paddle (this.game,this.world.centerX, 500);
        this.capsule_object = new capsule (this.game,this.world.centerX, this.paddle_object.y - 22);
        this.player_object = new player (this.game, 32, 550, 650, 550,this.world.centerX, 550);
        this.capsule_object.events.onOutOfBounds.add(this.capsuleLost, this);
        this.virus_object = new virus (this.game);
     
        this.input.onDown.add(this.releasecapsule, this);
        }

        update () {
        if (this.capsuleOnPaddle) {
            this.capsule_object.body.x = this.paddle_object.x - 14;
        }
        else {
            this.physics.arcade.collide(this.capsule_object , this.paddle_object, this.capsuleHitObject, null, this);
            this.physics.arcade.collide(this.capsule_object, this.virus_object, this.capsuleHitVirus, null, this);
        }
       
    }

    capsuleHitObject (_capsule, _object) : void {

        let diff = 0;

        if (_capsule.x < _object.x) {
            diff = _object.x - _capsule.x;
            _capsule.body.velocity.x = (-10 * diff);
        }
        else if (_capsule.x > _object.x) {
            diff = _capsule.x -_object.x;
            _capsule.body.velocity.x = (10 * diff);
        }
        else {
            _capsule.body.velocity.x = 2 + Math.random() * 8;
        }

    }

    releasecapsule ():void {
        if (this.capsuleOnPaddle) {
            this.capsuleOnPaddle = false;
            this.capsule_object.body.velocity.y = -300;
            this.capsule_object.body.velocity.x = -75;
            this.capsule_object.animations.play('spin');
            if (this.player_object.getStatus() != '') {
                this.player_object.setScore(0);
                this.player_object.setlives(3);
                this.player_object.setStatus('');
            }
        }
    }

    capsuleLost ():void {
        this.player_object.processLostLifePt();
        if (this.player_object.getlives() === 0) {
            this.capsuleOnPaddle = true;
            this.player_object.setStatus("GAME OVER - YOU LOSE :(");
            var tween = this.add.tween(this.player_object._statusValue).to({ y: 800 }, 2000, Phaser.Easing.Linear.None, true);
            tween.onComplete.add(this.GameOver, this);
        }
        else {
            this.capsuleOnPaddle = true;

            this.capsule_object.reset(this.paddle_object.body.x + 16, this.paddle_object.body.y - 16);
            
            this.capsule_object.animations.stop();
        }
    }

    GameOver ():void {
         
        this.game.state.start('gamemenu', true, false);            
    }
 
    capsuleHitVirus (_capsule, _virus) :void {
        _virus.kill();
        this.player_object.processScorePt();
        if (this.virus_object.countLiving() == 0) {
           this.capsuleOnPaddle = true;
           this.capsule_object.body.velocity.set(0);
           this.capsule_object.x = this.paddle_object.x + 16;
           this.capsule_object.y = this.paddle_object.y - 16;
           this.capsule_object.animations.stop();
           this.virus_object.forEachDead(function (virus_param) {virus_param.revive();}, this);
           this.player_object.setStatus("YOU WIN :)");
        }
    } 
}

export { level1 };