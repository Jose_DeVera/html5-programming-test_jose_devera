/*
Author: jose.ii.y.de.vera
Class Description: breakout class is main class of the game that contains 4 states.
*/

import {boot} from './com/states/boot';
import {preloader} from './com/states/preloader';
import {gamemenu} from './com/states/gamemenu';
import {level1} from './com/states/level1';

export class Breakout extends Phaser.Game {

    constructor() {
   
        super (800, 600, Phaser.AUTO, 'phaser-example',null);
        this.state.add('boot', boot, false);
        this.state.add('preloader', preloader, false);
        this.state.add('gamemenu', gamemenu, false);
        this.state.add('level1', level1, false);
        this.state.start('boot');
        
    }
}


