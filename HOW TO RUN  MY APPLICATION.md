
## How to run my Application ##

NOTE: SKIP Step 1 to 4 if you have project existed and nodejs already installed.
1. [Fork](../../fork) this repository to your bitbucket account, then clone it to your local machine
2. Install [nodejs](https://nodejs.org/en/download/)
3. Open a shell/command prompt in the repository root and run the following:
4. * Install dependencies
```
npm install
```
5. Copy the ts codes , images and folders to my repo accordingly
6. Command:
```
* Build project
```
npm run build
```
* Start hosting local webserver
```
npm start
```
6. Go to your web browser and 
Visit [localhost:8080](http://localhost:8080) in your browser to view the output.

