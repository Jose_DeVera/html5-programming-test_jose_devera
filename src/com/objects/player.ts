/*
Author: jose.ii.y.de.vera
Class Description: player class is a sprite class contains players attributes of score, life points and status message
to be used in all levels classes.
*/

class player extends Phaser.Sprite{
         
        private _lives:any = 3;
        private _lifeDeduct: any = 1;
        private _lifePointString:any = 'Life Points : ';
        private _lifeValue: Phaser.Text;
        private _score: any = 0;
        private _scorePoint: any = 5
        private _scorePointString: any = 'Score : ';
        private _scoreValue: Phaser.Text;
        private _statusMsg: any = '';
        public _statusValue: Phaser.Text;

        constructor(game: Phaser.Game, ScoreX: number, ScoreY: number, LifeX: number, LifeY:number, StatusX: number, StatusY: number) {
            //var tempString = _lifePointString+_lives;
            super(game, 0, 0, '', 0); 
            // var tempString = this._lifePointString+this._lives;
            this._scoreValue = this.game.add.text(ScoreX, ScoreY, this._scorePointString +this._score, { font: "20px Arial", fill: "#ffffff", align: "left" });
            this._lifeValue = this.game.add.text(LifeX, LifeY, this._lifePointString+this._lives, { font: "20px Arial", fill: "#ffffff", align: "left" });
            this._statusValue = this.game.add.text(StatusX, StatusY, this._statusMsg, { font: "20px Arial", fill: "#ffffff", align: "left" });
            this._statusValue.anchor.setTo(0.5,0.5);

            game.add.existing(this);
             
        }

        update() {
             this._scoreValue.text = this._scorePointString + this.getScore();
             this._lifeValue.text = this._lifePointString + this.getlives();
             this._statusValue.text = this.getStatus();
        }

        getlives():any {
            return this._lives;
        }
        
        setlives(lifePt:any) {
            this._lives = lifePt;
        }

        processLostLifePt():void{
            this.setlives(this._lives - this._lifeDeduct);
        }

        getScore ():any {
            return this._score;
        }

        setScore (scorePt:any){
            this._score = scorePt;
        }

        processScorePt(): void {
            this.setScore(this._score + this._scorePoint); 
        }

        getStatus ():any {
            return this._statusMsg;
        }

        setStatus (StatusMsg:any){
            this._statusMsg = StatusMsg;
        }
    }
export { player };