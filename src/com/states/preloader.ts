/*
Author: jose.ii.y.de.vera
Class Description: preload class is 2nd state class that 
contains loading images of the game
*/


class preloader extends Phaser.State {

    public preloadBar: Phaser.Sprite;

    preload() {
            this.load.image('logo', 'images/logo.png');
            this.load.image('title', 'images/title.png');
            this.load.image('paddle', 'images/paddle.png');
            this.load.image('capsule', 'images/capsule.png');
            this.load.atlas('virus', 'images/virus.png','images/virus.json');
            this.load.image('bg', 'images/red_blood_cell_bg1.jpg');
        }

        create() {
            this.preloadBar = this.add.sprite(this.world.centerX, this.world.centerY, 'preloadBar');
            this.preloadBar.anchor.setTo(0.5, 0.5);

            var tween = this.add.tween(this.preloadBar).to({ alpha: 0 }, 5000, Phaser.Easing.Circular.Out, true);
            tween.onComplete.add(this.startGameMenu, this);

        }    

        startGameMenu() {
            this.game.state.start('gamemenu', true, false);
            console.log ("preloader class in startMainMenu");
        }

    }

export { preloader }; 