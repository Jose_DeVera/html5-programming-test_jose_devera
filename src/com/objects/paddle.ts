/*
Author: jose.ii.y.de.vera
Class Description: paddle class is a sprite class contains paddle image 
to be used in all levels classes.
*/

class paddle extends Phaser.Sprite {
         
        constructor(game: Phaser.Game, x: number, y: number) {
 
            super(game, x, y, 'paddle', 0);
            
            game.physics.enable(this,Phaser.Physics.ARCADE);
            this.anchor.setTo(0.5, 0.5);
            this.body.collideWorldBounds = true;
            this.body.bounce.set(1);
            this.body.immovable = true;
            
            game.add.existing(this);
 
        }

        update() {
            this.x = this.game.input.x;

            if (this.x < 24)
            {
            this.x = 24;
            }
            else if (this.x> this.game.width - 24) 
            {
            this.x = this.game.width - 24;
            }    
        }
 
    }
export { paddle };