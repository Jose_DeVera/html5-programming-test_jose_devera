/*
Author: jose.ii.y.de.vera
Class Description: virus class is a group class that generate objects of virus to be used in
all level classes.
*/

class virus extends Phaser.Group {
         
        private virus_create: any; 

        constructor(game: Phaser.Game) {
            
            //super(game, x, y, 'ants', 0);
            super (game,game.world,'virus',true,true,Phaser.Physics.ARCADE);
            this.enableBody=true;           
            this.createObjectGroups();
     
            
            game.add.existing(this);    
        }

        createObjectGroups():void{
            for (var y = 0; y < 4; y++)
            {
                for (var x = 0; x < 6; x++)
                {
                this.virus_create = this.create(120 + (x * this.game.rnd.integerInRange(76, 110)), 100 + (y * this.game.rnd.integerInRange(56, 90)), 'virus', 'virus.png');
                //this.virus_create = this.create(120 + (x * 76), 100 + (y * 52), 'virus', 'virus.png');
                 this.virus_create.body.bounce.set(1);
                 this.virus_create.body.immovable = true;
                 this.virus_create.body.tint = Math.random() * 0xffffff;
                }
            }  
        }

        update () {
            
            for (var i = 0, len = this.children.length; i < len; i++) {  
                this.children[i].pivot.x = 105;
                if (0 == i%2){
                    this.children[i].rotation+=0.05;
                } else {
                    this.children[i].rotation-=0.05;
                }
            }
        }

        getVirusGroup ():any {
            return this.virus_create;
        }

        setVirusGroup (): any {
            return this.virus_create;
        }
 
    }
export { virus };