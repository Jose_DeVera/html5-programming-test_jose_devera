/*
Author: jose.ii.y.de.vera
Class Description: boot class is first state class that 
initialize the global game settings of the game in desktop or mobile mode.
*/


class boot extends Phaser.State {

    preload() {
 
            this.load.image('preloadBar', 'images/loading.png')
 
        }
 
        create() {
            //  Game Settings
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
 
            if (this.game.device.desktop) {
                //  desktop settings
                this.game.scale.pageAlignHorizontally = true;
            }
            else {
                //  mobile settings.
                this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                this.game.scale.minWidth = 480;
                this.game.scale.minHeight = 260;
                this.game.scale.maxWidth = 1024;
                this.game.scale.maxHeight = 768;
                this.game.scale.forceLandscape = true;
                this.game.scale.pageAlignHorizontally = true;
            }
 
            this.game.state.start('preloader', true, false);
        }
}
export { boot };