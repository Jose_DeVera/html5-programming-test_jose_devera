# README #

## How do I get set up? ##

* [Fork](../../fork) this repository to your bitbucket account, then clone it to your local machine
* Install [nodejs](https://nodejs.org/en/download/)

Open a shell/command prompt in the repository root and run the following:

* Install dependencies
```
npm install
```
* Build project
```
npm run build
```
* Start hosting local webserver
```
npm start
```

Visit [localhost:8080](http://localhost:8080) in your browser to view the output.

Leave the webserver running while you develop so you can test your changes. Rerun the build command to build, then hard-refresh the browser window.

