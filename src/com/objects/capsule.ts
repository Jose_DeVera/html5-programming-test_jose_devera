/*
Author: jose.ii.y.de.vera
Class Description: capsule class is a sprite class contains sprite capsule
to be used in all levels classes.
*/


class capsule extends Phaser.Sprite {
         
        ballOnPaddle : boolean = true;
        _paddlecoordinates : any;

        constructor(game: Phaser.Game, x: number, y: number) {
 
            super(game, x, y, 'capsule', 0);
            game.physics.enable(this,Phaser.Physics.ARCADE);
            this.anchor.set(0.5,0.5);
            this.checkWorldBounds = true;
            this.body.collideWorldBounds = true;
            this.body.bounce.set(1);
            game.add.existing(this);
 
        }
 
    }
export { capsule };